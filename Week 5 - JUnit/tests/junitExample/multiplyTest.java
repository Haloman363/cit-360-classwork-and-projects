package junitExample;

import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class multiplyTest {

    private multiply newMultiplication = new multiply();
    private int output = 0;
    private int output2 = 0;

    @Test
    public void assertEqualsMultiplyNumbers() {
        output = newMultiplication.multiplyNumbers(4,8);
        assertEquals(32, output);
    }

    @Test
    public void assertTrueMultiplyNumbers() {
        output = newMultiplication.multiplyNumbers(4,25);
        assertTrue(output == 100);
    }

    @Test
    public void assertFalseMultiplyNumbers() {
        output = newMultiplication.multiplyNumbers(2,30);
        assertFalse(output < 10);
    }

    @Test
    public void assertNotNullMultiplyNumbers() {
        output = newMultiplication.multiplyNumbers(1,1);
        assertNotNull(output);
    }

    @Test
    public void assertNullMultiplyNumbers() {
        String nullObject = null;
        assertNull(nullObject);
    }

    @Test
    public void assertSameMultiplyNumbers() {
        output = newMultiplication.multiplyNumbers(9,10);
        output2 = newMultiplication.multiplyNumbers(2,45);
        assertSame(output,output2);
    }

    @Test
    public void assertNotSameMultiplyNumbers() {
        output = newMultiplication.multiplyNumbers(90,10);
        output2 = newMultiplication.multiplyNumbers(2,24);
        assertNotSame(output,output2);
    }

    @Test
    public void assertArrayEqualsMultiplyNumbers() {
        output = newMultiplication.multiplyNumbers(10,4);
        output2 = newMultiplication.multiplyNumbers(5,8);
        int[] firstArray = new int[] {output,output2};
        output = newMultiplication.multiplyNumbers(2,20);
        output2 = newMultiplication.multiplyNumbers(8,5);
        int[] secondArray = new int[] {output,output2};
        assertArrayEquals(firstArray,secondArray);
    }

    @Test
    public void assertThatMultiplyNumbers() {
        output = newMultiplication.multiplyNumbers(10,4);
        output2 = newMultiplication.multiplyNumbers(5,8);
        Assert.assertThat(output, is(output2));
    }
}