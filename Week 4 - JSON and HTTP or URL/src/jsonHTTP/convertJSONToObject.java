package jsonHTTP;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class convertJSONToObject {
    public static Game toObject() throws ConnectException, MalformedURLException {

        String content = "";
        String urlString = "http://localhost:8080/week4JSON/data.json";

        try {
            System.out.println("\nAttempting to connect to server...");
            URL url = new URL(urlString);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line= reader.readLine()) != null){
                stringBuilder.append(line + "\n");
            }

            content = stringBuilder.toString();
            System.out.println("Retrieved JSON.");

        } catch (ConnectException e) {
            throw new ConnectException("Unable to connect to server.\nURL: " + urlString);
        } catch (MalformedURLException e) {
            throw new MalformedURLException("Please check URL for errors. Unable to connect.\nURL: " + urlString);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ObjectMapper mapper = new ObjectMapper();
        Game newGame = null;

        try {
            System.out.println("Converting JSON to Object...");
            newGame = mapper.readValue(content, Game.class);
            System.out.println("Conversion complete.");
        } catch (JsonProcessingException e){
            System.err.println(e.toString());
        }

        return newGame;
    }}

