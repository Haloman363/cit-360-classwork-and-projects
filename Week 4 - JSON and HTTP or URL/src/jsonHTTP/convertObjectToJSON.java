package jsonHTTP;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class convertObjectToJSON{
    public static String ToJSON(Game game) throws FileNotFoundException, ConnectException, MalformedURLException {

        ObjectMapper mapper = new ObjectMapper();
        String output = "";
        String pathLocation = "C:\\Users\\Jaymes\\Documents\\School\\Fall 2020\\Object Programming\\Apache Tom\\webapps\\week4JSON\\data.json";

        try {
            mapper.writeValue(new File(pathLocation), game);
            System.out.println("Saved object as JSON successfully to server folder.");
            output = mapper.writeValueAsString(game);
        } catch (FileNotFoundException e){
            throw new FileNotFoundException("Cannot access or find file path to save object as JSON\nPath: " + pathLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String content = "";
        String urlString = "http://localhost:8080/week4JSON/data.json";

        try {
            System.out.println("\nAttempting to connect to server...");
            URL url = new URL(urlString);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line= reader.readLine()) != null){
                stringBuilder.append(line + "\n");
            }

            output = stringBuilder.toString();
            System.out.println("JSON loaded successfully.\n");

        } catch (ConnectException e) {
            throw new ConnectException("Unable to connect to server.\nURL: " + urlString);
        } catch (MalformedURLException e) {
            throw new MalformedURLException("Please check URL for errors. Unable to connect.\nURL: " + urlString);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return output;
    }
}
