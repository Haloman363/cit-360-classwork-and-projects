package jsonHTTP;

public class Game {

    private String name;
    private double price;
    private String developer;
    private String url;
    private boolean consoleExclusive;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean getConsoleExclusive() {
        return consoleExclusive;
    }

    public void setConsoleExclusive(boolean consoleExclusive) {
        this.consoleExclusive = consoleExclusive;
    }

    @Override
    public String toString() {
        return "Name: " + name + "\nPrice: $" + price + "\nDeveloper: " + developer + "\nLink: " + url + "\nConsole Exclusive?: " + consoleExclusive;
    }
}
