package jsonHTTP;

import com.fasterxml.jackson.databind.ObjectMapper;

public class createJSONAndConvertObject {

    public static void main(String[] args) {
        Game game = new Game();

        game.setName("Halo");
        game.setPrice(59.99);
        game.setDeveloper("Bungie");
        game.setUrl("https://www.xbox.com/en-US/games/halo");
        game.setConsoleExclusive(true);

        System.out.println("Created the following game object:\n\n" + game.toString());
        System.out.println("\nConverting Object to JSON and saving to file.");

        try {
            String jsonString = convertObjectToJSON.ToJSON(game);

            System.out.println("JSON from the server:\n" + jsonString);

        }catch (Exception e){
                System.out.println(e.getMessage());
            }
        try {
            Game gameFromServer = convertJSONToObject.toObject();
            System.out.println("Created the following game object:\n\n" + gameFromServer.toString());
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
