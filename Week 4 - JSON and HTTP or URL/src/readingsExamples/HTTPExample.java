package readingsExamples;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class HTTPExample {

    public static String getHTTPContent(String string) {

        String content = "";

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line= reader.readLine()) != null){
                stringBuilder.append(line + "\n");
            }

            content = stringBuilder.toString();

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return content;
    }

    public static Map getHttpHeaders(String string){
        Map hashmap = null;

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            hashmap = http.getHeaderFields();

        } catch (Exception e){
            System.err.println(e.toString());
        }

        return hashmap;
    }

    public static void main(String[] args) {
        System.out.println(HTTPExample.getHTTPContent("http://www.google.com"));

        Map<Integer, List<String>> m = HTTPExample.getHttpHeaders("http://www.google.com");
        for (Map.Entry<Integer, List<String>> entry : m.entrySet()) {
            System.out.println("Key= " + entry.getKey() + " Value= " + entry.getValue());
        }
    }
}
