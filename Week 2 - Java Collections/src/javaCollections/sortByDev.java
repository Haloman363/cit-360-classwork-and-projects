package javaCollections;

import java.util.Comparator;

public class sortByDev implements Comparator<game> {
    public int compare(game a, game b){
        return a.dev.compareTo(b.dev);
    }
}
