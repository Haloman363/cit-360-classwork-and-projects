package javaCollections;

import java.io.IOException;
import java.util.*;

public class CollectionTypes {
    public static void main(String[] args) {

        System.out.println("Java Collection Types:\n");
        System.out.println("----List----");
        System.out.println("The following declares an ArrayList and adds 5 games to the list:\n");

        List games = new ArrayList();

        games.add("Halo");
        games.add("Black Ops");
        games.add("Rust");
        games.add("Minecraft");
        games.add("Rocket League");
        games.add("PUBG");

        for (Object str : games){
            System.out.println((String)str);
        }

        System.out.println("\nThe following removes the game 'PUBG' from the array:\n");
        games.remove("PUBG");
        for (Object str : games) {
            System.out.println((String) str);
        }

        System.out.println("\nThe size of the array is: " + games.size());
        System.out.println("The array contains the game 'Rust': " + games.contains("Rust"));

        games.clear();
        System.out.println("The array has now been cleared of all items: " + games.isEmpty());


        System.out.println("\n----Queue----");
        System.out.println("This takes the same items from list above and adds them in a PriorityQue() sorting them " +
                "\nalphabetically\n");
        Queue queue = new PriorityQueue();

        queue.add("Halo");
        queue.add("Black Ops");
        queue.add("Rust");
        queue.add("Minecraft");
        queue.add("Rocket League");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        System.out.println("\nWhen a duplicate item such as 'Halo' is added to the queue it is placed at the end of the " +
                "\nqueue. Also the value '3' is added to queue.\n");
        Queue queue2 = new LinkedList();

        queue2.add("Halo");
        queue2.add("Black Ops");
        queue2.add("Rust");
        queue2.add("Minecraft");
        queue2.add("Rocket League");

        queue2.add("Halo");
        queue2.add(3);

        Iterator iterator2 = queue2.iterator();
        while (iterator2.hasNext()) {
            System.out.println(queue2.poll());
        }

        System.out.println("\n----Set----");
        System.out.println("This creates a set with the previous values I have been working with but it attempts to add " +
                "\n'Halo' and 'Black Ops' twice and since they are duplicates they are not added. The following example" +
                "\nis a tree set:\n");

        TreeSet set = new TreeSet();

        set.add("Halo");
        set.add("Black Ops");
        set.add("Rust");
        set.add("Minecraft");
        set.add("Rocket League");
        set.add("Halo");
        set.add("Black Ops");

        for (Object str : set){
            System.out.println((String)str);
        }

        System.out.println("\nThe following example is a HashSet:\n");

        HashSet hashSet = new HashSet();

        hashSet.add("Halo");
        hashSet.add("Black Ops");
        hashSet.add("Rust");
        hashSet.add("Minecraft");
        hashSet.add("Rocket League");
        hashSet.add("Halo");
        hashSet.add("Black Ops");

        for (Object str : hashSet){
            System.out.println((String)str);
        }


        System.out.println("\n----Map----\n");
        Map map = new HashMap();
        map.put(1, "Halo");
        map.put(2, "Black Ops");
        map.put(3, "Rust");
        map.put(4, "Minecraft");
        map.put(5, "Rocket League");
        map.put(6, "Halo");


        for (int i = 1; i <= 6; i++) {
            String mapResult = (String) map.get(i);
            System.out.println(mapResult);
        }

        System.out.println("\nRemoving the third item by key from the map:\n");
        map.remove(3);

        for (int i = 1; i <= map.size(); i++) {
            String mapResult = (String) map.get(i);
            System.out.println(mapResult);
        }


        System.out.println("\n----List Using Generics----\n");

        List<game> gamesList = new LinkedList<game>();

        gamesList.add(new game("Halo","Bungie",60));
        gamesList.add(new game("Rocket League","Psyonix",20));
        gamesList.add(new game("Black Ops","Treyarch",60));
        gamesList.add(new game("Minecraft","Microsoft",30));
        gamesList.add(new game("Rust","FacePunch",40));

        for (game output : gamesList) {
            System.out.println(output);
        }

        System.out.println("\n----List Sorted With Comparator----");
        System.out.println("List from above sorted by price:\n");

        Collections.sort(gamesList, new sortByPrice());
        for (int i=0; i<gamesList.size(); i++){
            System.out.println(gamesList.get(i));
        }

        System.out.println("\nList from above sorted by developer:\n");

        Collections.sort(gamesList, new sortByDev());
        for (int i=0; i<gamesList.size(); i++){
            System.out.println(gamesList.get(i));
        }
    }
}
