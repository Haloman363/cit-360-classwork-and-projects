package javaCollections;

import java.util.Comparator;

public class sortByPrice implements Comparator<game> {

    public int compare(game a, game b){
        return b.amount - a.amount;
    }
}
