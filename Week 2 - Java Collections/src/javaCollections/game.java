package javaCollections;

public class game {

    private String title;
    public String dev;
    public int amount;

    public game(String title, String dev, int amount) {
        this.title = title;
        this.dev = dev;
        this.amount = amount;
    }

    public String toString() {
        return "Title: " + title + " | Developer: " + dev + " | Amount: " + amount;
    }

}
