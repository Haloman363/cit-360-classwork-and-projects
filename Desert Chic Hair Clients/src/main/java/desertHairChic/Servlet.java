package desertHairChic;

import org.hibernate.Session;

import javax.persistence.criteria.CriteriaBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.List;

@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String firstNameString = ((request.getParameter("firstName")).trim()).replaceAll("\\s+", "");
        String lastNameString = ((request.getParameter("lastName")).trim()).replaceAll("\\s+", "");
        String ageString = ((request.getParameter("age")).trim()).replaceAll("\\s+", "");
        String preferredStyleString = (request.getParameter("preferredStyle")).trim();
        String preferredLengthString = ((request.getParameter("preferredLength")).trim()).replaceAll("\\s+", "");
        String trimBladeNumberString = ((request.getParameter("trimBladeNumber")).trim()).replaceAll("\\s+", "");

        if ((firstNameString == "") | (lastNameString == "") | (preferredStyleString == "")) {
            htmlOutput(out, "badStrings");
        } else if ((preferredLengthString.length() > 200) | (firstNameString.length() > 45) | (lastNameString.length() > 45) ){
            htmlOutput(out, "tooLongOfStrings");
        } else{
            int ageInt;
            float preferredLengthFloat;
            float trimBladeNumberFloat;

            try {
                ageInt = Integer.parseInt(ageString);
                preferredLengthFloat = Float.parseFloat(preferredLengthString);
                trimBladeNumberFloat = Float.parseFloat(trimBladeNumberString);

                    Customer newCustomer = new Customer();

                    newCustomer.setFirstName(firstNameString);
                    newCustomer.setLastName(lastNameString);
                    newCustomer.setAge(ageInt);
                    newCustomer.setPreferredStyle(preferredStyleString);
                    newCustomer.setPreferredLength(preferredLengthFloat);
                    newCustomer.setTrimBladeNum(trimBladeNumberFloat);

                    Session session = hibernateUtils.getSessionFactory().openSession();
                    session.beginTransaction();
                    session.save(newCustomer);
                    session.getTransaction().commit();

                    htmlOutput(out, "addComplete");
                    doGet(request, response);
            } catch (NumberFormatException e) {
                htmlOutput(out, "badNumbers");
            }
        }
    }

    private void htmlOutput(PrintWriter out, String typeOfMessage) {
        if (typeOfMessage == "addComplete"){
            out.println("<div><p class='addSuccess'>The new client was added successfully!</p></div>");
        }
        if (typeOfMessage == "badNumbers"){
            out.println("<html><head><link rel=\"stylesheet\" href=\"main.css\"></head><body><div class='errorDiv'>");
            out.println("<h1>Error</h1>");
            out.println("<p class='firstRow errorP'>Please make sure only numbers are entered for the client's age, preferred length, and trim blade number.</p>");
            out.println("<button class='goBack' onclick=\"goBack()\">Back</button><script>function goBack(){window.history.back();}</script>");
            out.println("</body></html>");
            out.println("</div></body></html>");
        }
        if (typeOfMessage == "badStrings"){
            out.println("<html><head><link rel=\"stylesheet\" href=\"main.css\"></head><body><div class='errorDiv'>");
            out.println("<h1>Error</h1>");
            out.println("<p class='firstRow errorP'>Please make sure text is entered for the client's first name, last name, and preferred style.</p>");
            out.println("<button class='goBack' onclick=\"goBack()\">Back</button><script>function goBack(){window.history.back();}</script>");
            out.println("</body></html>");
            out.println("</div></body></html>");
        }
        if (typeOfMessage == "tooLongOfStrings"){
            out.println("<html><head><link rel=\"stylesheet\" href=\"main.css\"></head><body><div class='errorDiv'>");
            out.println("<h1>Error</h1>");
            out.println("<p class='firstRow errorP'>Please make sure the firstname and lastname are 45 characters or less and the preferred style notes are less than 200 characters.</p>");
            out.println("<button class='goBack' onclick=\"goBack()\">Back</button><script>function goBack(){window.history.back();}</script>");
            out.println("</body></html>");
            out.println("</div></body></html>");
        }
    }

    private void displayClients(PrintWriter out){
        out.println("<div class='currentClientsDiv'><h2>Current Clients Info:</h2>");

        try {
            dataAccessLayer t = desertHairChic.dataAccessLayer.getInstance();
            List<Customer> customerList = t.getCustomer();

            if (customerList.size() == 0) {
                out.println("<hr>");
                out.println("<p>No clients currently saved. Add one using the boxes above!</p>");
            }else {
                out.println("<hr>");
                out.println("<ul>");

                for (Customer i : customerList) {
                    out.println("<li class='clientName'>" + i.getFirstName() + " " + i.getLastName()
                            + "<p>Age: " + i.getAge() + "<p>"
                            + "<p>Preferred Length (inches): " + i.getPreferredLength() + "<p>"
                            + "<p>Preferred Style: " + i.getPreferredStyle() + "<p>"
                            + "<p>Trim Blade Number (ie: 1, 1.25): " + i.getTrimBladeNum() + "<p>"
                            + "</li>");
                    out.println("<hr>");
                }
                out.println("</ul>");
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head><link rel=\"stylesheet\" href=\"main.css\"><body><h1>Desert Chic Hair Clients</h1>" +
                "<form action=\"Servlet\" method=\"post\">\n" +
                "    <label class=\"firstRow\">First Name:</label>\n" +
                "    <input class=\"firstRow\" name=\"firstName\" type=\"text\" autofocus=\"true\"/>\n" +
                "    <label class=\"secondRow\">Last Name:</label>\n" +
                "    <input class=\"secondRow\" name=\"lastName\" type=\"text\">" +
                "    <label class='thirdRow'>Age:</label>\n" +
                "    <input class=\"thirdRow\" name=\"age\" type=\"text\"/>" +
                "    <label class=\"fourthRow\">Preferred Style:</label>\n" +
                "    <input class=\"fourthRow\" name=\"preferredStyle\" type=\"text\">" +
                "    <label class=\"fifthRow\">Preferred Length (inches):</label>\n" +
                "    <input class=\"fifthRow\" name=\"preferredLength\" type=\"text\"/>" +
                "    <label class=\"sixthRow\">Trim Blade Number (ie: 1, 1.25):</label>\n" +
                "    <input class=\"sixthRow\" name=\"trimBladeNumber\" type=\"text\"/>" +
                "    <input class=\"submit\" type=\"submit\" value=\"Add Client\"/>" +
                "     </form>");

        displayClients(out);

        out.println("</div>");
        out.println("</body></html>");
        }
    }
