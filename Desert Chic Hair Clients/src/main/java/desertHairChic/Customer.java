package desertHairChic;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "customer", uniqueConstraints = {
        @UniqueConstraint(columnNames = "customerId")
})

public class Customer implements Serializable {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "customerId", unique = true, nullable = false)
        private int id;

        @Column(name = "firstName", length = 45)
        private String firstName;

        @Column(name = "lastName", nullable = false, length = 45)
        private String lastName;

        @Column(name ="age", nullable = false)
        private int age;

        @Column(name = "preferredStyle", nullable = false, length = 45)
        private String preferredStyle;

        @Column(name = "preferredLength", nullable = false)
        private float preferredLength;
        
        @Column(name = "trimBladeNum", nullable = false)
        private float trimBladeNum;

        public int getCustomerId() {
            return id;
        }

        public void setCustomerId(int id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }
        
        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public int getAge(){
            return age;
        }

        public void setAge(int age){
            this.age = age;
        }

        public String getPreferredStyle() {
            return preferredStyle;
        }

        public void setPreferredStyle(String preferredStyle) {
            this.preferredStyle = preferredStyle;
        }

        public float getPreferredLength() {
        return preferredLength;
    }

        public void setPreferredLength(float preferredLength) {
        this.preferredLength = preferredLength;
    }

        public float getTrimBladeNum() {
        return trimBladeNum;
    }

        public void setTrimBladeNum(float trimBladeNum) {
        this.trimBladeNum = trimBladeNum;
    }

        public String toString() {
            return ("\nFirst Name: " + firstName + "\nLast Name: " + lastName + "\nPreferred Style: " + preferredStyle + "\nPreferred Length: " +
                    preferredLength + "\nTrim Blade Number: " + trimBladeNum);
        }
}
