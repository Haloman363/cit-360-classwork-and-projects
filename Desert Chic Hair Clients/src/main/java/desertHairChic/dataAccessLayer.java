package desertHairChic;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class dataAccessLayer {

    SessionFactory factory;
    Session session = null;

    private static dataAccessLayer single_instance = null;

    private dataAccessLayer()
    {
        factory = hibernateUtils.getSessionFactory();
    }

    public static dataAccessLayer getInstance()
    {
        if (single_instance == null) {
            single_instance = new dataAccessLayer();
        }

        return single_instance;
    }

    public List<Customer> getCustomer() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from desertHairChic.Customer";
            List<Customer> cs = (List<Customer>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;
        }
        catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }
}
