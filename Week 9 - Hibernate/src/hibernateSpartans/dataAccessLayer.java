package hibernateSpartans;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

public class dataAccessLayer {

    SessionFactory factory;
    Session session = null;

    private static dataAccessLayer single_instance = null;

    private dataAccessLayer()
    {
        factory = hibernateUtils.getSessionFactory();
    }

    public static dataAccessLayer getInstance()
    {
        if (single_instance == null) {
            single_instance = new dataAccessLayer();
        }

        return single_instance;
    }

    public List<Spartan> getSpartan() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from hibernateSpartans.Spartan";
            List<Spartan> cs = (List<Spartan>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        }
        catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    public Spartan getSpartan(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from hibernateSpartans.Spartan where id=" + Integer.toString(id);
            Spartan c = (Spartan) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}
