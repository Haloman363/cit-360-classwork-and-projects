package hibernateSpartans;

import javax.persistence.NoResultException;
import java.util.*;

public class runHibernateSpartans {

    public static void main(String[] args) {

        boolean restartProgram = true;

        do{
            dataAccessLayer t = dataAccessLayer.getInstance();
            List<Spartan> spartanList = t.getSpartan();
            System.out.println("\nList of current Spartans:\n");

            for (Spartan i : spartanList) {
                System.out.println(i.getId() + ". " +i.getName());
            }

            boolean inputCorrect = false;
            int input;

            do{
                try{
                    input = getInputs();
                    if (input > spartanList.size() || input == 0){
                        throw new NoResultException("\nThat number could not be found in the database. Please verify that number entered was listed.");
                    }
                    System.out.println(t.getSpartan(input));
                    inputCorrect = true;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }while (!inputCorrect);

            restartProgram = restartMain();

        } while (restartProgram == true);


    }

    private static boolean restartMain() {
        Scanner restartInput = new Scanner(System.in);
        System.out.println("\nWould you like to view another spartan(yes/no): ");
        String restartCheck = restartInput.nextLine();
        restartCheck = restartCheck.trim();

        if (restartCheck.equalsIgnoreCase("yes") | restartCheck.equalsIgnoreCase("y")) {
            return true;
        } else {
            System.out.println("\nThank you for using my program!");
            return false;
        }
    }

    private static int getInputs() throws Exception {
        Scanner input = new Scanner(System.in);
        int checkedInput = 0;

        try {
            System.out.print("\nSelect a Spartan to view more details: ");
            String idNumber = input.nextLine();
            idNumber = idNumber.replaceAll("\\s+", "");
            checkedInput = Integer.parseInt(idNumber);
        } catch (NumberFormatException e) {
            throw new Exception("\nThe entry is not a number. Please re-enter.");
        }

        return checkedInput;
    }
}
