package calcThreads;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class executeCalcThreads {
    public static void main(String[] args) {
        ExecutorService myService = Executors.newFixedThreadPool(2);

        float[] inputs = new float[2];
        boolean inputCorrect = false;

        do{
            try{
                inputs = getInputs();
                inputCorrect = true;
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }while (!inputCorrect);

        calcNumbers calc1 = new calcNumbers(inputs[0], inputs[1], "Thread 1");
        calcNumbers calc2 = new calcNumbers((inputs[0]+10), (inputs[1]+10), "Thread 2");
        calcNumbers calc3 = new calcNumbers((inputs[0]+30), (inputs[1]+30), "Thread 3");
        calcNumbers calc4 = new calcNumbers((inputs[0]+100), (inputs[1]+100), "Thread 4");
        calcNumbers calc5 = new calcNumbers((inputs[0]+10000), (inputs[1]+10000), "Thread 5");

        myService.execute(calc1);
        myService.execute(calc2);
        myService.execute(calc3);
        myService.execute(calc4);
        myService.execute(calc5);

        myService.shutdown();
    }

    private static float[] getInputs() throws Exception {
        Scanner input1 = new Scanner(System.in);
        Scanner input2 = new Scanner(System.in);

        float[] inputs = new float[2];
        boolean input2Correct = false;

        try {
            System.out.print("Please enter the first number: ");
            String firstNumber = input1.nextLine();
            firstNumber = firstNumber.replaceAll("\\s+", "");
            inputs[0] = Float.parseFloat(firstNumber);
        } catch (Exception e) {
            throw new Exception("\nThe first entry is not a number. Please re-enter.");
        }

            try {
                    System.out.print("Please enter the second number: ");
                    String secondNumber = input2.nextLine();
                    secondNumber = secondNumber.replaceAll("\\s+", "");
                    inputs[1] = Float.parseFloat(secondNumber);
                    input2Correct = true;
                    System.out.println();
            } catch (Exception e) {
                throw new Exception("\nThe second entry is not a number. Please re-enter.");
            }
        return inputs;
    }
}