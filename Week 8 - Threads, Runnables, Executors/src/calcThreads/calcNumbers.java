package calcThreads;

public class calcNumbers implements Runnable {
    private float firstNumber;
    private float secondNumber;
    private String threadNum;

    public calcNumbers(float firstInput, float secondInput, String threadNum){
        this.firstNumber = firstInput;
        this.secondNumber = secondInput;
        this.threadNum = threadNum;
    }

    public float addNumbers(){
        return this.firstNumber + this.secondNumber;
    }

    public float subNumbers(){
        return this.firstNumber - this.secondNumber;
    }

    public float multNumbers(){
        return  this.firstNumber * this.secondNumber;
    }

    public String divNumbers(){
        if (this.secondNumber != 0){
            return String.valueOf(this.firstNumber / this.secondNumber);
        }else {
            return "Cannot divide by 0.";
        }
    }

    @Override
    public void run() {
        System.out.println(this.threadNum + " " +this.firstNumber + " + " + this.secondNumber + " = " + this.addNumbers() + "\n");
        System.out.println(this.threadNum + " " +this.firstNumber + " - " + this.secondNumber + " = " + this.subNumbers() + "\n");
        System.out.println(this.threadNum + " " +this.firstNumber + " * " + this.secondNumber + " = " + this.multNumbers() + "\n");
        System.out.println(this.threadNum + " " +this.firstNumber + " / " + this.secondNumber + " = " + this.divNumbers() + "\n");

        System.out.println(this.threadNum + " is complete.");
    }
}
