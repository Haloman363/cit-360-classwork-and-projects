package readingsExamples;

import java.util.Random;

public class doingSomething implements Runnable {

    private String name;
    private int number;
    private int sleep;
    private int rand;

    public doingSomething(String name, int number, int sleep){
        this.name = name;
        this.number = number;
        this.sleep = sleep;

        Random random = new Random();
        this.rand = random.nextInt(1000);
    }

    public void run() {
        System.out.print("\n\nExecuting with these parameters: Name = " + name + " Number = " + number + " Sleep = " + sleep
        + " Rand Num = " + rand + "\n\n");

        for (int count = 1; count < rand; count++){
            if (count % number == 0){
                System.out.print(name + " is sleeping, ");
                try{
                    Thread.sleep(sleep);
                }catch (InterruptedException e){
                    System.err.println(e.toString());
                }
            }
        }
        System.out.println("\n\n" + name + " is done.\n\n");
    }
}
