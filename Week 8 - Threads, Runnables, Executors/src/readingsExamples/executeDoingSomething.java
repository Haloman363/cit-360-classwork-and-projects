package readingsExamples;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class executeDoingSomething {

    public static void main(String[] args) {
        ExecutorService myService = Executors.newFixedThreadPool(3);

        doingSomething ds1 = new doingSomething("Bob", 25, 1000);
        doingSomething ds2 = new doingSomething("Jim", 10, 500);
        doingSomething ds3 = new doingSomething("John", 5, 250);
        doingSomething ds4 = new doingSomething("Emily", 2, 100);
        doingSomething ds5 = new doingSomething("Bruce", 1, 50);

        myService.execute(ds1);
        myService.execute(ds2);
        myService.execute(ds3);
        myService.execute(ds4);
        myService.execute(ds5);

        myService.shutdown();
    }
}
