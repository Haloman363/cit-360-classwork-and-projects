package exceptionHandlingAndDataValidation;

import java.util.Scanner;

public class divideNumbers {
    public static void main(String[] args) {
        boolean restartProgram = true;

        do {
            float[] inputs = new float[2];
            float outputTotal = 0;

            boolean inputCorrect = false;

            do {
                try {
                    inputs = getInputs();
                    inputCorrect = true;

                    //Code below no longer need with data validation:
//                    try {
                        outputTotal = doDivision(inputs[0], inputs[1]);
//                    } catch (ArithmeticException e) {
//                        System.out.println(e.getMessage());
//                        inputCorrect = false;
//                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            } while (!inputCorrect);


            System.out.println("The answer is: " + outputTotal);

            restartProgram = restartMain();

        } while (restartProgram = true);

    }

    private static boolean restartMain() {
        Scanner restartInput = new Scanner(System.in);
        System.out.println("\nWould you like to divide more numbers (yes/no): ");
        String restartCheck = restartInput.nextLine();
        restartCheck = restartCheck.trim();

        if (restartCheck.equalsIgnoreCase("yes") | restartCheck.equalsIgnoreCase("y")) {
            return true;
        } else {
            System.out.println("\nThank you for using my program!");
            return false;
        }
    }

    private static float[] getInputs() throws Exception {
        Scanner input1 = new Scanner(System.in);
        Scanner input2 = new Scanner(System.in);

        float[] inputs = new float[2];
        boolean notZero = false;

            try {
                System.out.print("Please enter the first number: ");
                String firstNumber = input1.nextLine();
                firstNumber = firstNumber.replaceAll("\\s+", "");
                inputs[0] = Float.parseFloat(firstNumber);
            } catch (Exception e) {
                throw new Exception("\nThe first entry is not a number. Please re-enter.");
            }

        do {
            try {
                System.out.print("Please enter the second number: ");
                String secondNumber = input2.nextLine();
                secondNumber = secondNumber.replaceAll("\\s+", "");
                inputs[1] = Float.parseFloat(secondNumber);
                notZero = true;
            } catch (Exception e) {
                throw new Exception("\nThe second entry is not a number. Please re-enter.");
            }
            if (inputs[1] == 0){
                System.out.println("Can not divide by zero.\n");
                notZero = false;
            }
        } while (notZero == false);

        return inputs;
    }

    private static float doDivision(float num1, float num2)  {
        //Original code below for throwing exception of dividing by zero:
        //throws ArithmeticException
//        if (num2 == 0) {
//            throw new ArithmeticException("Cannot divide by zero. Please re-enter the numbers to be divided.\n");
//        } else {
            return num1 / num2;
//        }
    }
}
