package simpleServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParsePosition;

@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String firstNumString = ((request.getParameter("firstNum")).trim()).replaceAll("\\s+", "");
        String secondNumString = ((request.getParameter("secondNum")).trim()).replaceAll("\\s+", "");

        float firstNum = 0;
        float secondNum = 0;
        boolean inputCorrect = false;
        DecimalFormat df = new DecimalFormat();

        try {
            firstNum = Float.parseFloat(firstNumString);
            secondNum = Float.parseFloat(secondNumString);
            inputCorrect = true;
        } catch (NumberFormatException e){
            htmlOutput("error", out, firstNumString, secondNumString, BigDecimal.valueOf(0));
        }
        if (inputCorrect == true){
            BigDecimal total = BigDecimal.valueOf(calcExpo(firstNum,secondNum));
            String firstNumberOutput = String.valueOf(df.parse(String.valueOf(firstNum), new ParsePosition(0)));
            String secondNumberOutput = String.valueOf(df.parse(String.valueOf(secondNum), new ParsePosition(0)));
            htmlOutput("complete", out, firstNumberOutput, secondNumberOutput, total);
        }
    }

    private void htmlOutput(String type, PrintWriter out, String first, String second, BigDecimal solution) {
        if (type == "complete"){
            out.println("<html><head><link rel=\"stylesheet\" href=\"main.css\"></head><body><div>");
            out.println("<h1>Output</h1>");
            out.println("<p class='firstRow'>The Base: " + first + "</p>");
            out.println("<p class='secondRow'>To the Power of: " + second + "</p>");
            out.println("<p class='thirdRow'>Is: " + solution + "</p>");
            out.println("<button class='goBack' onclick=\"goBack()\">Back</button><script>function goBack(){window.history.back();}</script>");
            out.println("</body></html>");
            out.println("</div></body></html>");
        }else if (type == "error"){
            out.println("<html><head><link rel=\"stylesheet\" href=\"main.css\"></head><body><div>");
            out.println("<h1>Output</h1>");
            out.println("<p class='firstRow'>Please make sure only numbers are entered. You entered:</p>");
            out.println("<p class='secondRow'>The Base: " + first + "</p>");
            out.println("<p class='thirdRow noBackGround'>To the Power of: " + second + "</p>");
            out.println("<button class='goBack' onclick=\"goBack()\">Back</button><script>function goBack(){window.history.back();}</script>");
            out.println("</body></html>");
            out.println("</div></body></html>");
        }
    }

    private float calcExpo(float base, float expo) {
        float runningTotal = 0;
        for (int i = 0; i <= expo; i++){
                if (i == 0 && expo == 0){
                    runningTotal = base;
                    break;
                }else if (i == 0){
                    runningTotal = base * base;
                    i = 2;
                } else {
                    runningTotal = runningTotal * base;
                }
            }
        return runningTotal;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head><body>");
        out.println("<h1>Something went wrong</h1>");
        out.println("<p>This resource is not available directly.</p>");
        out.println("<button onclick=\"goBack()\">Go Back</button><script>function goBack(){window.history.back();}</script>");
        out.println("</body></html>");
    }
}
